"use strict";

System.register(["aurelia-framework"], function (_export, _context) {
    "use strict";

    var computedFrom, _createClass, _dec, _dec2, _dec3, _desc, _value, _class, Letter;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
        var desc = {};
        Object['ke' + 'ys'](descriptor).forEach(function (key) {
            desc[key] = descriptor[key];
        });
        desc.enumerable = !!desc.enumerable;
        desc.configurable = !!desc.configurable;

        if ('value' in desc || desc.initializer) {
            desc.writable = true;
        }

        desc = decorators.slice().reverse().reduce(function (desc, decorator) {
            return decorator(target, property, desc) || desc;
        }, desc);

        if (context && desc.initializer !== void 0) {
            desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
            desc.initializer = undefined;
        }

        if (desc.initializer === void 0) {
            Object['define' + 'Property'](target, property, desc);
            desc = null;
        }

        return desc;
    }

    return {
        setters: [function (_aureliaFramework) {
            computedFrom = _aureliaFramework.computedFrom;
        }],
        execute: function () {
            _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            _export("Letter", Letter = (_dec = computedFrom('compRes'), _dec2 = computedFrom('compRes'), _dec3 = computedFrom('selected'), (_class = function () {
                function Letter() {
                    var ch = arguments.length <= 0 || arguments[0] === undefined ? "" : arguments[0];
                    var rnd = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

                    _classCallCheck(this, Letter);

                    this.ch = ch;
                    this.f1o = "";
                    this.f2o = "";
                    this.f3o = "";
                    this.f1l = "";
                    this.f2l = "";
                    this.f3l = "";
                    this.comp = "";
                    this.compRes = 0;

                    this.selected = false;

                    if (rnd) this.set_random();
                }

                Letter.prototype._compare = function _compare(l, o) {
                    var vari;
                    var f1l = parseInt(l);
                    var f1o = parseInt(o);
                    if (!f1l || !f1o) {
                        return 0;
                    }
                    if (f1l < f1o) vari = f1l / f1o;else {
                        vari = (f1l - f1o) / f1o;
                        if (vari <= 1) vari = 1 - vari;
                    }

                    if (vari > 1) vari = 0;else if (vari < 0) vari = 0;

                    return vari * 100;
                };

                Letter.prototype._compareTwo = function _compareTwo(l1, l2, o1, o2) {
                    var comp1 = this._compare(l1, o1);
                    var comp2 = this._compare(l2, o2);
                    return (comp1 + comp2) / 2;
                };

                Letter.prototype.compare = function compare(indication) {
                    var set = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

                    var res = 0;
                    switch (indication) {
                        case "f1":
                            res = this._compare(this.f1l, this.f1o);
                            if (set) this.comp = "F1";
                            break;
                        case "f2":
                            res = this._compare(this.f2l, this.f2o);
                            if (set) this.comp = "F2";
                            break;
                        case "f3":
                            res = this._compare(this.f3l, this.f3o);
                            if (set) this.comp = "F3";
                            break;
                        case "f1f2":
                            res = this._compareTwo(this.f1l, this.f2l, this.f1o, this.f2o);
                            if (set) this.comp = "F1,F2";
                            break;
                        case "f2f3":
                            res = this._compareTwo(this.f2l, this.f3l, this.f2o, this.f3o);
                            if (set) this.comp = "F2,F3";
                            break;
                        case "f1f3":
                            res = this._compareTwo(this.f1l, this.f3l, this.f1o, this.f3o);
                            if (set) this.comp = "F1,F3";
                            break;
                    }
                    res = Math.abs(Math.round(res));
                    if (set) this.compRes = res;
                    return res;
                };

                Letter.prototype.set_random = function set_random() {
                    this.f1o = Math.round(Math.random() * 1000);
                    this.f2o = Math.round(Math.random() * 3000);
                    this.f3o = Math.round(Math.random() * 3000);
                    this.f1l = Math.round(Math.random() * this.f1o / 3) + this.f1o;
                    this.f2l = Math.round(Math.random() * this.f2o / 3) + this.f2o;
                    this.f3l = Math.round(Math.random() * this.f3o / 3) + this.f3o;
                };

                _createClass(Letter, [{
                    key: "is_success",
                    get: function get() {
                        return this.progress_class.includes("progress-bar-success");
                    }
                }, {
                    key: "progress_class",
                    get: function get() {
                        var vari = this.compRes;
                        var prog = "progress-bar ";
                        vari = vari < 0 ? -vari : vari;
                        if (vari < 50) prog += "progress-bar-danger";else if (vari < 65) prog += "progress-bar-rose";else if (vari < 80) prog += "progress-bar-warning";else if (vari < 90) prog += "progress-bar-lightgreen";else {
                            prog += "progress-bar-success";
                        }
                        return prog;
                    }
                }, {
                    key: "selected_class",
                    get: function get() {
                        return this.selected ? 'selected' : '';
                    }
                }]);

                return Letter;
            }(), (_applyDecoratedDescriptor(_class.prototype, "is_success", [_dec], Object.getOwnPropertyDescriptor(_class.prototype, "is_success"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "progress_class", [_dec2], Object.getOwnPropertyDescriptor(_class.prototype, "progress_class"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "selected_class", [_dec3], Object.getOwnPropertyDescriptor(_class.prototype, "selected_class"), _class.prototype)), _class)));

            _export("Letter", Letter);
        }
    };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxldHRlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFRLHdCLHFCQUFBLFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs4QkFFSyxNLFdBd0ZSLGFBQWEsU0FBYixDLFVBS0EsYUFBYSxTQUFiLEMsVUFtQkEsYUFBYSxVQUFiLEM7QUE5R0Qsa0NBQWtDO0FBQUEsd0JBQXRCLEVBQXNCLHlEQUFqQixFQUFpQjtBQUFBLHdCQUFiLEdBQWEseURBQVAsS0FBTzs7QUFBQTs7QUFDOUIseUJBQUssRUFBTCxHQUFVLEVBQVY7QUFDQSx5QkFBSyxHQUFMLEdBQVcsRUFBWDtBQUNBLHlCQUFLLEdBQUwsR0FBVyxFQUFYO0FBQ0EseUJBQUssR0FBTCxHQUFXLEVBQVg7QUFDQSx5QkFBSyxHQUFMLEdBQVcsRUFBWDtBQUNBLHlCQUFLLEdBQUwsR0FBVyxFQUFYO0FBQ0EseUJBQUssR0FBTCxHQUFXLEVBQVg7QUFDQSx5QkFBSyxJQUFMLEdBQVksRUFBWjtBQUNBLHlCQUFLLE9BQUwsR0FBZSxDQUFmOztBQUVBLHlCQUFLLFFBQUwsR0FBZ0IsS0FBaEI7O0FBRUEsd0JBQUcsR0FBSCxFQUNJLEtBQUssVUFBTDtBQUNQOztpQ0FFRCxRLHFCQUFTLEMsRUFBRyxDLEVBQUc7QUFDWCx3QkFBSSxJQUFKO0FBQ0Esd0JBQUksTUFBTSxTQUFTLENBQVQsQ0FBVjtBQUNBLHdCQUFJLE1BQU0sU0FBUyxDQUFULENBQVY7QUFDQSx3QkFBRyxDQUFDLEdBQUQsSUFBUSxDQUFDLEdBQVosRUFBaUI7QUFDYiwrQkFBTyxDQUFQO0FBQ0g7QUFDRCx3QkFBRyxNQUFNLEdBQVQsRUFDSSxPQUFPLE1BQU0sR0FBYixDQURKLEtBRUs7QUFDRCwrQkFBTyxDQUFDLE1BQU0sR0FBUCxJQUFjLEdBQXJCO0FBQ0EsNEJBQUcsUUFBUSxDQUFYLEVBQ0ksT0FBTyxJQUFJLElBQVg7QUFDUDs7QUFFRCx3QkFBRyxPQUFPLENBQVYsRUFBYSxPQUFPLENBQVAsQ0FBYixLQUNLLElBQUcsT0FBTyxDQUFWLEVBQWEsT0FBTyxDQUFQOztBQUVsQiwyQkFBTyxPQUFPLEdBQWQ7QUFDSCxpQjs7aUNBRUQsVyx3QkFBWSxFLEVBQUksRSxFQUFJLEUsRUFBSSxFLEVBQUk7QUFDeEIsd0JBQUksUUFBUSxLQUFLLFFBQUwsQ0FBYyxFQUFkLEVBQWtCLEVBQWxCLENBQVo7QUFDQSx3QkFBSSxRQUFRLEtBQUssUUFBTCxDQUFjLEVBQWQsRUFBa0IsRUFBbEIsQ0FBWjtBQUNBLDJCQUFPLENBQUMsUUFBUSxLQUFULElBQWtCLENBQXpCO0FBQ0gsaUI7O2lDQUVELE8sb0JBQVEsVSxFQUF5QjtBQUFBLHdCQUFiLEdBQWEseURBQVAsS0FBTzs7QUFDN0Isd0JBQUksTUFBTSxDQUFWO0FBQ0EsNEJBQU8sVUFBUDtBQUNJLDZCQUFLLElBQUw7QUFDSSxrQ0FBTSxLQUFLLFFBQUwsQ0FBYyxLQUFLLEdBQW5CLEVBQXdCLEtBQUssR0FBN0IsQ0FBTjtBQUNBLGdDQUFHLEdBQUgsRUFBUSxLQUFLLElBQUwsR0FBWSxJQUFaO0FBQ1I7QUFDSiw2QkFBSyxJQUFMO0FBQ0ksa0NBQU0sS0FBSyxRQUFMLENBQWMsS0FBSyxHQUFuQixFQUF3QixLQUFLLEdBQTdCLENBQU47QUFDQSxnQ0FBRyxHQUFILEVBQVEsS0FBSyxJQUFMLEdBQVksSUFBWjtBQUNSO0FBQ0osNkJBQUssSUFBTDtBQUNJLGtDQUFNLEtBQUssUUFBTCxDQUFjLEtBQUssR0FBbkIsRUFBd0IsS0FBSyxHQUE3QixDQUFOO0FBQ0EsZ0NBQUcsR0FBSCxFQUFRLEtBQUssSUFBTCxHQUFZLElBQVo7QUFDUjtBQUNKLDZCQUFLLE1BQUw7QUFDSSxrQ0FBTSxLQUFLLFdBQUwsQ0FBaUIsS0FBSyxHQUF0QixFQUEyQixLQUFLLEdBQWhDLEVBQXFDLEtBQUssR0FBMUMsRUFBK0MsS0FBSyxHQUFwRCxDQUFOO0FBQ0EsZ0NBQUcsR0FBSCxFQUFRLEtBQUssSUFBTCxHQUFZLE9BQVo7QUFDUjtBQUNKLDZCQUFLLE1BQUw7QUFDSSxrQ0FBTSxLQUFLLFdBQUwsQ0FBaUIsS0FBSyxHQUF0QixFQUEyQixLQUFLLEdBQWhDLEVBQXFDLEtBQUssR0FBMUMsRUFBK0MsS0FBSyxHQUFwRCxDQUFOO0FBQ0EsZ0NBQUcsR0FBSCxFQUFRLEtBQUssSUFBTCxHQUFZLE9BQVo7QUFDUjtBQUNKLDZCQUFLLE1BQUw7QUFDSSxrQ0FBTSxLQUFLLFdBQUwsQ0FBaUIsS0FBSyxHQUF0QixFQUEyQixLQUFLLEdBQWhDLEVBQXFDLEtBQUssR0FBMUMsRUFBK0MsS0FBSyxHQUFwRCxDQUFOO0FBQ0EsZ0NBQUcsR0FBSCxFQUFRLEtBQUssSUFBTCxHQUFZLE9BQVo7QUFDUjtBQXhCUjtBQTBCQSwwQkFBTSxLQUFLLEdBQUwsQ0FBUyxLQUFLLEtBQUwsQ0FBVyxHQUFYLENBQVQsQ0FBTjtBQUNBLHdCQUFHLEdBQUgsRUFBUSxLQUFLLE9BQUwsR0FBZSxHQUFmO0FBQ1IsMkJBQU8sR0FBUDtBQUNILGlCOztpQ0FFRCxVLHlCQUFhO0FBQ1QseUJBQUssR0FBTCxHQUFXLEtBQUssS0FBTCxDQUFXLEtBQUssTUFBTCxLQUFjLElBQXpCLENBQVg7QUFDQSx5QkFBSyxHQUFMLEdBQVcsS0FBSyxLQUFMLENBQVcsS0FBSyxNQUFMLEtBQWMsSUFBekIsQ0FBWDtBQUNBLHlCQUFLLEdBQUwsR0FBVyxLQUFLLEtBQUwsQ0FBVyxLQUFLLE1BQUwsS0FBYyxJQUF6QixDQUFYO0FBQ0EseUJBQUssR0FBTCxHQUFXLEtBQUssS0FBTCxDQUFXLEtBQUssTUFBTCxLQUFjLEtBQUssR0FBbkIsR0FBdUIsQ0FBbEMsSUFBdUMsS0FBSyxHQUF2RDtBQUNBLHlCQUFLLEdBQUwsR0FBVyxLQUFLLEtBQUwsQ0FBVyxLQUFLLE1BQUwsS0FBYyxLQUFLLEdBQW5CLEdBQXVCLENBQWxDLElBQXVDLEtBQUssR0FBdkQ7QUFDQSx5QkFBSyxHQUFMLEdBQVcsS0FBSyxLQUFMLENBQVcsS0FBSyxNQUFMLEtBQWMsS0FBSyxHQUFuQixHQUF1QixDQUFsQyxJQUF1QyxLQUFLLEdBQXZEO0FBQ0gsaUI7Ozs7d0NBR2dCO0FBQ2IsK0JBQU8sS0FBSyxjQUFMLENBQW9CLFFBQXBCLENBQTZCLHNCQUE3QixDQUFQO0FBQ0g7Ozt3Q0FHb0I7QUFDakIsNEJBQUksT0FBTyxLQUFLLE9BQWhCO0FBQ0EsNEJBQUksT0FBTyxlQUFYO0FBQ0EsK0JBQU8sT0FBTyxDQUFQLEdBQVcsQ0FBQyxJQUFaLEdBQW1CLElBQTFCO0FBQ0EsNEJBQUcsT0FBTyxFQUFWLEVBQ0ksUUFBUSxxQkFBUixDQURKLEtBRUssSUFBRyxPQUFPLEVBQVYsRUFDRCxRQUFRLG1CQUFSLENBREMsS0FFQSxJQUFHLE9BQU8sRUFBVixFQUNELFFBQVEsc0JBQVIsQ0FEQyxLQUVBLElBQUcsT0FBTyxFQUFWLEVBQ0QsUUFBUSx5QkFBUixDQURDLEtBRUE7QUFDRCxvQ0FBUSxzQkFBUjtBQUNIO0FBQ0QsK0JBQU8sSUFBUDtBQUNIOzs7d0NBR29CO0FBQ2pCLCtCQUFPLEtBQUssUUFBTCxHQUFnQixVQUFoQixHQUE2QixFQUFwQztBQUNIIiwiZmlsZSI6ImxldHRlci5qcyIsInNvdXJjZVJvb3QiOiIvc3JjIn0=
