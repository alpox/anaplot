'use strict';

System.register(['aurelia-framework', 'letter', 'd3', 'jquery', 'jquery-ui', 'bootstrap'], function (_export, _context) {
    "use strict";

    var computedFrom, inject, observable, Letter, d3, $, _createClass, _dec, _dec2, _dec3, _desc, _value, _class, _descriptor, _descriptor2, _descriptor3, App;

    function _initDefineProp(target, property, descriptor, context) {
        if (!descriptor) return;
        Object.defineProperty(target, property, {
            enumerable: descriptor.enumerable,
            configurable: descriptor.configurable,
            writable: descriptor.writable,
            value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
        });
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
        var desc = {};
        Object['ke' + 'ys'](descriptor).forEach(function (key) {
            desc[key] = descriptor[key];
        });
        desc.enumerable = !!desc.enumerable;
        desc.configurable = !!desc.configurable;

        if ('value' in desc || desc.initializer) {
            desc.writable = true;
        }

        desc = decorators.slice().reverse().reduce(function (desc, decorator) {
            return decorator(target, property, desc) || desc;
        }, desc);

        if (context && desc.initializer !== void 0) {
            desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
            desc.initializer = undefined;
        }

        if (desc.initializer === void 0) {
            Object['define' + 'Property'](target, property, desc);
            desc = null;
        }

        return desc;
    }

    function _initializerWarningHelper(descriptor, context) {
        throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');
    }

    return {
        setters: [function (_aureliaFramework) {
            computedFrom = _aureliaFramework.computedFrom;
            inject = _aureliaFramework.inject;
            observable = _aureliaFramework.observable;
        }, function (_letter) {
            Letter = _letter.Letter;
        }, function (_d) {
            d3 = _d;
        }, function (_jquery) {
            $ = _jquery.default;
        }, function (_jqueryUi) {}, function (_bootstrap) {}],
        execute: function () {
            _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            _export('App', App = (_dec = computedFrom('compAll'), _dec2 = computedFrom('compAll'), _dec3 = computedFrom('currentAnalysis', 'nameO', 'nameL'), (_class = function () {
                function App() {
                    _classCallCheck(this, App);

                    this.showMouth = false;
                    this.showAreaO = false;
                    this.showAreaL = false;
                    this.showCompArea = false;
                    this.currentData = [];
                    this.title = 'QualiPhon';

                    _initDefineProp(this, 'currentLetters', _descriptor, this);

                    this.currentAnalysis = 0;
                    this.compArea = 0;
                    this.compAreaSide = 0;
                    this.compAll = 0;
                    this.compDesc = "";
                    this.lang = "";

                    _initDefineProp(this, 'nameO', _descriptor2, this);

                    _initDefineProp(this, 'nameL', _descriptor3, this);

                    this.letters = [new Letter("a", true), new Letter("b", true), new Letter("c", true), new Letter("d", true), new Letter("e", true), new Letter("f", true), new Letter("g", true), new Letter("h", true), new Letter("i", true), new Letter("j", true), new Letter("k", true), new Letter("l", true), new Letter("m", true), new Letter("n", true), new Letter("o", true), new Letter("p", true), new Letter("q", true), new Letter("r", true), new Letter("s", true), new Letter("t", true), new Letter("u", true), new Letter("v", true), new Letter("w", true), new Letter("x", true), new Letter("y", true), new Letter("z", true)];
                }

                App.prototype.attached = function attached() {
                    $('.draggable').draggable();
                    $('.draggable').resizable();
                };

                App.prototype.compare = function compare(comp) {
                    this.currentLetters.forEach(function (currentLetter) {
                        if (!currentLetter) return;
                        currentLetter.compare(comp, true);
                    });
                };

                App.prototype.compareAll = function compareAll(comp) {
                    var _this = this;

                    var res = 0;
                    var all = function all(func) {
                        return _this.letters.reduce(function (b, c) {
                            return b + func(c);
                        }, 0) / _this.letters.length;
                    };
                    switch (comp) {
                        case "f1f2":
                            res = all(function (l) {
                                return l.compare("f1f2");
                            });
                            this.compDesc = "All F1,F2";
                            break;
                        case "f1f3":
                            res = all(function (l) {
                                var c1 = l.compare("f1");
                                var c2 = l.compare("f2");
                                var c3 = l.compare("f3");
                                return (c1 + c2 + c3) / 3;
                            });
                            this.compDesc = "All F1-F3";
                            break;
                    }
                    this.compAll = Math.abs(Math.round(res));
                };

                App.prototype.toggleArea = function toggleArea() {
                    this.showCompArea = !this.showCompArea;
                };

                App.prototype.compareArea = function compareArea() {
                    var dataO = this._dataO();
                    var dataL = this._dataL();
                    var hullO = d3.geom.hull(dataO.map(function (e) {
                        return [e.x, e.y];
                    }));
                    var hullL = d3.geom.hull(dataL.map(function (e) {
                        return [e.x, e.y];
                    }));
                    var areaO = d3.geom.polygon(hullO).area();
                    var areaL = d3.geom.polygon(hullL).area();

                    var max = d3.max([areaO, areaL]);
                    var min = d3.min([areaO, areaL]);

                    if (max === areaO) this.compAreaSide = 1;else this.compAreaSide = 2;

                    this.compArea = Math.abs(Math.round(min / max * 100));
                };

                App.prototype.removeLetter = function removeLetter(index) {
                    var conf = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

                    if (! ~index) return;
                    var letter = this.letters[index];
                    if (conf) {
                        var confirmation = confirm("Are you sure that you want to remove this letter?");
                        if (confirmation) this.letters.splice(index, 1);
                    } else this.letters.splice(index, 1);
                };

                App.prototype.removeSelected = function removeSelected() {
                    var _this2 = this;

                    var confirmation = confirm("Are you sure you want to remove all selected letters?");
                    if (confirmation) this.currentLetters.forEach(function (l) {
                        return _this2.removeLetter(_this2.letters.indexOf(l), false);
                    });
                    this.currentLetters = [];
                };

                App.prototype.addLetter = function addLetter() {
                    this.letters.push(new Letter());
                };

                App.prototype.toggleMouth = function toggleMouth() {
                    this.showMouth = !this.showMouth;
                };

                App.prototype.choose = function choose(e, letter) {
                    var letters = this.currentLetters;
                    var index = this.letters.indexOf(letter);
                    var toggleSelected = function toggleSelected(l) {
                        if (l.selected) {
                            var idx = letters.indexOf(l);
                            letters.splice(idx, 1);
                            l.selected = false;
                        } else {
                            l.selected = true;
                            letters.push(l);
                        }
                    };
                    if (e.ctrlKey || e.metaKey) toggleSelected(letter);else if (e.shiftKey) {
                        var minIdx = Math.min(index, this.lastIndex);
                        var maxIdx = Math.max(index, this.lastIndex) + 1;
                        var part = this.letters.slice(minIdx, maxIdx);
                        part = part.filter(function (l) {
                            return ! ~letters.indexOf(l);
                        });
                        part.forEach(function (l) {
                            l.selected = true;
                            letters.push(l);
                        });
                    } else {
                        letters.forEach(function (l) {
                            return l.selected = false;
                        });
                        letters.length = 0;
                        letter.selected = true;
                        letters.push(letter);
                    }
                    this.lastIndex = this.letters.indexOf(letter);
                };

                App.prototype._progress_class = function _progress_class(vari) {
                    var prog = "progress-bar ";
                    vari = vari < 0 ? -vari : vari;
                    if (vari <= 50) prog += "progress-bar-danger";else if (vari < 65) prog += "progress-bar-rose";else if (vari < 80) prog += "progress-bar-warning";else if (vari < 90) prog += "progress-bar-lightgreen";else {
                        prog += "progress-bar-success";
                    }
                    return prog;
                };

                App.prototype.exportDump = function exportDump() {
                    var _this3 = this;

                    var mapLetter = function mapLetter(l) {
                        return {
                            ch: l.ch,
                            f1o: l.f1o,
                            f2o: l.f2o,
                            f3o: l.f3o,
                            f1l: l.f1l,
                            f2l: l.f2l,
                            f3l: l.f3l,
                            comp: l.comp,
                            compRes: l.compRes,
                            selected: l.selected
                        };
                    };
                    var data = {
                        lang: this.lang,
                        nameO: this.nameO,
                        nameL: this.nameL,
                        showMouth: this.showMouth,
                        currentAnalysis: this.currentAnalysis,
                        currentLetters: this.currentLetters.map(function (l) {
                            return _this3.letters.indexOf(l);
                        }),
                        compAll: this.compAll,
                        compDesc: this.compDesc,
                        letters: this.letters.map(mapLetter)
                    };
                    var json = JSON.stringify(data);
                    var fileContent = "text/json;charset=UTF-8," + encodeURIComponent(json);
                    var date = new Date();
                    var fileName = "qualiphon_export_" + date.getTime() + ".json";
                    var a = document.createElement('a');
                    a.href = 'data:' + fileContent;
                    a.download = fileName;
                    a.click();
                };

                App.prototype.importDump = function importDump() {
                    var _this4 = this;

                    var $files = $('#file');
                    var file = $files[0].files[0];
                    if (!file) {
                        alert("No file chosen.");
                        return;
                    }
                    var reader = new FileReader();
                    var mapLetters = function mapLetters(lets) {
                        var letters = [];
                        for (var i = 0; i < lets.length; i++) {
                            var l = lets[i];
                            var newLetter = new Letter();
                            newLetter.ch = l.ch;
                            newLetter.f1o = l.f1o;
                            newLetter.f2o = l.f2o;
                            newLetter.f3o = l.f3o;
                            newLetter.f1l = l.f1l;
                            newLetter.f2l = l.f2l;
                            newLetter.f3l = l.f3l;
                            newLetter.comp = l.comp, newLetter.compRes = l.compRes, newLetter.selected = l.selected;
                            letters.push(newLetter);
                        }
                        return letters;
                    };
                    reader.onload = function (e) {
                        var res = JSON.parse(e.target.result);
                        _this4.lang = res.lang;
                        _this4.nameO = res.nameO;
                        _this4.nameL = res.nameL;
                        _this4.letters = [];
                        _this4.showMouth = res.showMouth;
                        _this4.letters = mapLetters(res.letters);
                        _this4.currentAnalysis = res.currentAnalysis;
                        _this4.currentLetters = res.currentLetters.map(function (l) {
                            return _this4.letters[l];
                        });
                        _this4.compAll = res.compAll;
                        _this4.compDesc = res.compDesc;
                    };
                    reader.readAsText(file, "UTF-8");
                };

                App.prototype.fileChanged = function fileChanged(e) {
                    this.fileName = e.target.files[0].name;
                    this.importDump();
                };

                App.prototype.currentLettersChanged = function currentLettersChanged(newValue, oldValue) {
                    this.updateChart();
                };

                App.prototype.nameOChanged = function nameOChanged(newValue, oldValue) {
                    this.updateChart();
                };

                App.prototype.nameLChanged = function nameLChanged(newValue, oldValue) {
                    this.updateChart();
                };

                App.prototype.updateChart = function updateChart() {
                    switch (this.currentAnalysis) {
                        case 1:
                            return this.analyseO();
                        case 2:
                            return this.analyseL();
                        case 3:
                            return this.analyseOL();
                    }
                };

                App.prototype._dataO = function _dataO() {
                    var lineData = [];
                    lineData = this.currentLetters.map(function (letter) {
                        return {
                            x: letter.f2o,
                            y: letter.f1o,
                            letter: letter.ch,
                            which: 1
                        };
                    });
                    return lineData;
                };

                App.prototype._dataL = function _dataL() {
                    var lineData = [];
                    lineData = this.currentLetters.map(function (letter) {
                        return {
                            x: letter.f2l,
                            y: letter.f1l,
                            letter: letter.ch,
                            which: 2
                        };
                    });
                    return lineData;
                };

                App.prototype.analyseO = function analyseO() {
                    this.currentAnalysis = 1;
                    this._analyse(this._dataO());
                };

                App.prototype.analyseL = function analyseL() {
                    this.currentAnalysis = 2;
                    this._analyse(this._dataL());
                };

                App.prototype.analyseOL = function analyseOL() {
                    var lineData = [];
                    lineData = this._dataO().concat(this._dataL());
                    this.currentAnalysis = 3;
                    this._analyse(lineData);
                };

                App.prototype.areaO = function areaO() {
                    this.showAreaO = !this.showAreaO;
                    this._analyse();
                };

                App.prototype.areaL = function areaL() {
                    this.showAreaL = !this.showAreaL;
                    this._analyse();
                };

                App.prototype.areaOL = function areaOL() {
                    if (this.showAreaL || this.showAreaO) {
                        this.showAreaO = false;
                        this.showAreaL = false;
                    } else {
                        this.showAreaO = true;
                        this.showAreaL = true;
                    }
                    this._analyse();
                };

                App.prototype._analyse = function _analyse(lineData) {
                    var _this5 = this;

                    if (lineData) this.currentData = lineData;
                    lineData = this.currentData;
                    if (!lineData || !lineData.length) return;

                    d3.select("#chart").selectAll("*").remove();

                    var vis = d3.select("#chart"),
                        WIDTH = 600,
                        HEIGHT = 300,
                        MARGINS = {
                        top: 20,
                        right: 50,
                        bottom: 20,
                        left: 20
                    },
                        xRange = d3.scale.linear().range([MARGINS.left, WIDTH - MARGINS.right]).domain([3500, 0]),
                        yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([1200, 0]),
                        xAxis = d3.svg.axis().scale(xRange).orient("bottom").tickSize(1).ticks(7).innerTickSize(-HEIGHT + MARGINS.bottom + MARGINS.top).outerTickSize(0).tickPadding(10).tickSubdivide(true),
                        yAxis = d3.svg.axis().scale(yRange).orient("right").tickSize(1).ticks(7).innerTickSize(-WIDTH + MARGINS.left + MARGINS.right).outerTickSize(0).tickPadding(10).tickSubdivide(true);

                    var groupData = lineData.filter(function (d) {
                        return _this5.showAreaO && d.which == 1 || _this5.showAreaL && d.which == 2;
                    });
                    var hullGroups = d3.nest().key(function (d) {
                        return d.which;
                    }).entries(groupData);
                    var dGroups = function dGroups(d) {
                        return "M" + d3.geom.hull(d.values.map(function (e) {
                            return [xRange(e.x), yRange(e.y)];
                        })).join("L") + "Z";
                    };

                    var hull = vis.selectAll("path").data(hullGroups).attr("d", dGroups).enter().insert("path").style("fill", function (d) {
                        return d.key == 2 ? "#8bc34a" : "#03a9f4";
                    }).style("fill-opacity", "0.4").style("stroke", function (d) {
                        return d.key == 2 ? "#8bc34a" : "#03a9f4";
                    }).style("stroke-width", 2).attr("d", dGroups);

                    var xGroup = vis.append('svg:g').attr('class', 'x axis').attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')');

                    xGroup.call(xAxis);

                    xGroup.append('text').attr("y", 50).attr("x", WIDTH / 2).attr("style", "font-weight: bold; font-size: 14pt").text("F2");

                    var yGroup = vis.append('svg:g').attr('class', 'y axis').attr('transform', 'translate(' + (WIDTH - MARGINS.right) + ',0)');

                    yGroup.call(yAxis);

                    yGroup.append('text').attr("y", HEIGHT / 2).attr("x", 50).attr("style", "font-weight: bold; font-size: 14pt").text("F1");

                    var lineFunc = d3.svg.line().x(function (d) {
                        return xRange(d.x);
                    }).y(function (d) {
                        return yRange(d.y);
                    }).interpolate('linear');

                    var enterCircle = vis.selectAll("circle").data(lineData).enter();

                    var circleGroup = enterCircle.append("g");

                    circleGroup.append("circle").attr("cx", function (d) {
                        return xRange(d.x);
                    }).attr("cy", function (d) {
                        return yRange(d.y);
                    }).style("fill", function (d) {
                        return d.which === 1 ? "#3f51b5" : "#4caf50";
                    }).attr("r", 5);

                    circleGroup.append("text").attr("x", function (d) {
                        return xRange(d.x) + 10;
                    }).attr("y", function (d) {
                        return yRange(d.y) + 5;
                    }).text(function (d) {
                        return d.letter;
                    });

                    var legendData = [{ color: "#3f51b5", title: this.nameO, offset: 0 }, { color: "#4caf50", title: this.nameL, offset: 1 }];

                    var legend = vis.append("g").attr("class", "legend").attr("transform", "translate(" + WIDTH + "," + MARGINS.top + ")");

                    var enterLegend = legend.selectAll(".entry").data(legendData).enter();

                    var entry = enterLegend.append("g").attr("class", "entry").attr("transform", function (d) {
                        return "translate(10," + (d.offset * 20 + 10) + ")";
                    }).attr("y", function (d) {
                        return d.offset * 20;
                    });

                    entry.append("circle").attr("r", 7).style("fill", function (d) {
                        return d.color;
                    });

                    entry.append("text").attr("x", 20).attr("y", 7).text(function (d) {
                        return d.title;
                    });

                    vis.selectAll("circle").text(function (d) {
                        return d.letter;
                    });
                };

                App.prototype._randomColor = function _randomColor() {
                    var letters = '0123456789ABCDEF'.split('');
                    var color = '#';
                    for (var i = 0; i < 6; i++) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                };

                _createClass(App, [{
                    key: 'is_success',
                    get: function get() {
                        return this.progress_class.includes("progress-bar-success");
                    }
                }, {
                    key: 'progress_class',
                    get: function get() {
                        return this._progress_class(this.compAll);
                    }
                }, {
                    key: 'analysisName',
                    get: function get() {
                        switch (this.currentAnalysis) {
                            case 1:
                                return this.nameO;
                            case 2:
                                return this.nameL;
                            case 3:
                                return this.nameO + "/" + this.nameL;
                            default:
                                return "";
                        }
                    }
                }]);

                return App;
            }(), (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'currentLetters', [observable], {
                enumerable: true,
                initializer: function initializer() {
                    return [];
                }
            }), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'nameO', [observable], {
                enumerable: true,
                initializer: function initializer() {
                    return "O";
                }
            }), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, 'nameL', [observable], {
                enumerable: true,
                initializer: function initializer() {
                    return "L";
                }
            }), _applyDecoratedDescriptor(_class.prototype, 'is_success', [_dec], Object.getOwnPropertyDescriptor(_class.prototype, 'is_success'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'progress_class', [_dec2], Object.getOwnPropertyDescriptor(_class.prototype, 'progress_class'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'analysisName', [_dec3], Object.getOwnPropertyDescriptor(_class.prototype, 'analysisName'), _class.prototype)), _class)));

            _export('App', App);
        }
    };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQVEsd0IscUJBQUEsWTtBQUFjLGtCLHFCQUFBLE07QUFDZCxzQixxQkFBQSxVOztBQUNBLGtCLFdBQUEsTTs7QUFDSSxjOztBQUNMLGE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsyQkFJTSxHLFdBZ0tSLGFBQWEsU0FBYixDLFVBS0EsYUFBYSxTQUFiLEMsVUFzQkEsYUFBYSxpQkFBYixFQUFnQyxPQUFoQyxFQUF5QyxPQUF6QyxDOzs7O3lCQTFMSCxTLEdBQVksSzt5QkFDWixTLEdBQVksSzt5QkFDWixTLEdBQVksSzt5QkFDWixZLEdBQWUsSzt5QkFDZixXLEdBQWMsRTt5QkFDZCxLLEdBQVEsVzs7Ozt5QkFFUixlLEdBQWtCLEM7eUJBQ2xCLFEsR0FBVyxDO3lCQUNYLFksR0FBZSxDO3lCQUNmLE8sR0FBVSxDO3lCQUNWLFEsR0FBVyxFO3lCQUNYLEksR0FBTyxFOzs7Ozs7eUJBR1AsTyxHQUFVLENBQ04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQURNLEVBRU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQUZNLEVBR04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQUhNLEVBSU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQUpNLEVBS04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQUxNLEVBTU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQU5NLEVBT04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQVBNLEVBUU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQVJNLEVBU04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQVRNLEVBVU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQVZNLEVBV04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQVhNLEVBWU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQVpNLEVBYU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQWJNLEVBY04sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQWRNLEVBZU4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQWZNLEVBZ0JOLElBQUksTUFBSixDQUFXLEdBQVgsRUFBZ0IsSUFBaEIsQ0FoQk0sRUFpQk4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQWpCTSxFQWtCTixJQUFJLE1BQUosQ0FBVyxHQUFYLEVBQWdCLElBQWhCLENBbEJNLEVBbUJOLElBQUksTUFBSixDQUFXLEdBQVgsRUFBZ0IsSUFBaEIsQ0FuQk0sRUFvQk4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQXBCTSxFQXFCTixJQUFJLE1BQUosQ0FBVyxHQUFYLEVBQWdCLElBQWhCLENBckJNLEVBc0JOLElBQUksTUFBSixDQUFXLEdBQVgsRUFBZ0IsSUFBaEIsQ0F0Qk0sRUF1Qk4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQXZCTSxFQXdCTixJQUFJLE1BQUosQ0FBVyxHQUFYLEVBQWdCLElBQWhCLENBeEJNLEVBeUJOLElBQUksTUFBSixDQUFXLEdBQVgsRUFBZ0IsSUFBaEIsQ0F6Qk0sRUEwQk4sSUFBSSxNQUFKLENBQVcsR0FBWCxFQUFnQixJQUFoQixDQTFCTSxDOzs7OEJBNEJWLFEsdUJBQVc7QUFDUCxzQkFBRSxZQUFGLEVBQWdCLFNBQWhCO0FBQ0Esc0JBQUUsWUFBRixFQUFnQixTQUFoQjtBQUNILGlCOzs4QkFFRCxPLG9CQUFRLEksRUFBTTtBQUNWLHlCQUFLLGNBQUwsQ0FBb0IsT0FBcEIsQ0FBNEIseUJBQWlCO0FBQ3pDLDRCQUFHLENBQUMsYUFBSixFQUFtQjtBQUNuQixzQ0FBYyxPQUFkLENBQXNCLElBQXRCLEVBQTRCLElBQTVCO0FBQ0gscUJBSEQ7QUFJSCxpQjs7OEJBRUQsVSx1QkFBVyxJLEVBQU07QUFBQTs7QUFDYix3QkFBSSxNQUFNLENBQVY7QUFDQSx3QkFBSSxNQUFNLFNBQU4sR0FBTSxDQUFDLElBQUQsRUFBVTtBQUNoQiwrQkFBTyxNQUFLLE9BQUwsQ0FBYSxNQUFiLENBQW9CLFVBQUMsQ0FBRCxFQUFJLENBQUosRUFBVTtBQUNqQyxtQ0FBTyxJQUFJLEtBQUssQ0FBTCxDQUFYO0FBQ0gseUJBRk0sRUFFSixDQUZJLElBRUMsTUFBSyxPQUFMLENBQWEsTUFGckI7QUFHSCxxQkFKRDtBQUtBLDRCQUFPLElBQVA7QUFDSSw2QkFBSyxNQUFMO0FBQ00sa0NBQU0sSUFBSTtBQUFBLHVDQUFLLEVBQUUsT0FBRixDQUFVLE1BQVYsQ0FBTDtBQUFBLDZCQUFKLENBQU47QUFDQSxpQ0FBSyxRQUFMLEdBQWdCLFdBQWhCO0FBQ0E7QUFDTiw2QkFBSyxNQUFMO0FBQ00sa0NBQU0sSUFBSSxhQUFLO0FBQ1Asb0NBQUksS0FBSyxFQUFFLE9BQUYsQ0FBVSxJQUFWLENBQVQ7QUFDQSxvQ0FBSSxLQUFLLEVBQUUsT0FBRixDQUFVLElBQVYsQ0FBVDtBQUNBLG9DQUFJLEtBQUssRUFBRSxPQUFGLENBQVUsSUFBVixDQUFUO0FBQ0EsdUNBQU8sQ0FBQyxLQUFLLEVBQUwsR0FBVSxFQUFYLElBQWlCLENBQXhCO0FBQTRCLDZCQUo5QixDQUFOO0FBS0EsaUNBQUssUUFBTCxHQUFnQixXQUFoQjtBQUNBO0FBWlY7QUFjQSx5QkFBSyxPQUFMLEdBQWUsS0FBSyxHQUFMLENBQVMsS0FBSyxLQUFMLENBQVcsR0FBWCxDQUFULENBQWY7QUFDSCxpQjs7OEJBRUQsVSx5QkFBYTtBQUNULHlCQUFLLFlBQUwsR0FBb0IsQ0FBQyxLQUFLLFlBQTFCO0FBQ0gsaUI7OzhCQUVELFcsMEJBQWM7QUFDVix3QkFBSSxRQUFRLEtBQUssTUFBTCxFQUFaO0FBQ0Esd0JBQUksUUFBUSxLQUFLLE1BQUwsRUFBWjtBQUNBLHdCQUFJLFFBQVEsR0FBRyxJQUFILENBQVEsSUFBUixDQUFhLE1BQU0sR0FBTixDQUFVO0FBQUEsK0JBQUssQ0FBQyxFQUFFLENBQUgsRUFBTSxFQUFFLENBQVIsQ0FBTDtBQUFBLHFCQUFWLENBQWIsQ0FBWjtBQUNBLHdCQUFJLFFBQVEsR0FBRyxJQUFILENBQVEsSUFBUixDQUFhLE1BQU0sR0FBTixDQUFVO0FBQUEsK0JBQUssQ0FBQyxFQUFFLENBQUgsRUFBTSxFQUFFLENBQVIsQ0FBTDtBQUFBLHFCQUFWLENBQWIsQ0FBWjtBQUNBLHdCQUFJLFFBQVEsR0FBRyxJQUFILENBQVEsT0FBUixDQUFnQixLQUFoQixFQUF1QixJQUF2QixFQUFaO0FBQ0Esd0JBQUksUUFBUSxHQUFHLElBQUgsQ0FBUSxPQUFSLENBQWdCLEtBQWhCLEVBQXVCLElBQXZCLEVBQVo7O0FBRUEsd0JBQUksTUFBTSxHQUFHLEdBQUgsQ0FBTyxDQUFDLEtBQUQsRUFBUSxLQUFSLENBQVAsQ0FBVjtBQUNBLHdCQUFJLE1BQU0sR0FBRyxHQUFILENBQU8sQ0FBQyxLQUFELEVBQVEsS0FBUixDQUFQLENBQVY7O0FBRUEsd0JBQUcsUUFBUSxLQUFYLEVBQWtCLEtBQUssWUFBTCxHQUFvQixDQUFwQixDQUFsQixLQUNLLEtBQUssWUFBTCxHQUFvQixDQUFwQjs7QUFFTCx5QkFBSyxRQUFMLEdBQWdCLEtBQUssR0FBTCxDQUFTLEtBQUssS0FBTCxDQUFXLE1BQU0sR0FBTixHQUFZLEdBQXZCLENBQVQsQ0FBaEI7QUFDSCxpQjs7OEJBRUQsWSx5QkFBYSxLLEVBQW9CO0FBQUEsd0JBQWIsSUFBYSx5REFBTixJQUFNOztBQUMvQix3QkFBRyxFQUFDLENBQUMsS0FBTCxFQUFZO0FBQ1osd0JBQUksU0FBUyxLQUFLLE9BQUwsQ0FBYSxLQUFiLENBQWI7QUFDQSx3QkFBRyxJQUFILEVBQVM7QUFDTCw0QkFBSSxlQUFlLFFBQVEsbURBQVIsQ0FBbkI7QUFDQSw0QkFBRyxZQUFILEVBQ0ksS0FBSyxPQUFMLENBQWEsTUFBYixDQUFvQixLQUFwQixFQUEyQixDQUEzQjtBQUNQLHFCQUpELE1BS0ksS0FBSyxPQUFMLENBQWEsTUFBYixDQUFvQixLQUFwQixFQUEyQixDQUEzQjtBQUNMLGlCOzs4QkFFRCxjLDZCQUFpQjtBQUFBOztBQUNmLHdCQUFJLGVBQWUsUUFBUSx1REFBUixDQUFuQjtBQUNBLHdCQUFHLFlBQUgsRUFDSSxLQUFLLGNBQUwsQ0FBb0IsT0FBcEIsQ0FBNEI7QUFBQSwrQkFBSyxPQUFLLFlBQUwsQ0FBa0IsT0FBSyxPQUFMLENBQWEsT0FBYixDQUFxQixDQUFyQixDQUFsQixFQUEyQyxLQUEzQyxDQUFMO0FBQUEscUJBQTVCO0FBQ0oseUJBQUssY0FBTCxHQUFzQixFQUF0QjtBQUNELGlCOzs4QkFFRCxTLHdCQUFZO0FBQ1IseUJBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsSUFBSSxNQUFKLEVBQWxCO0FBQ0gsaUI7OzhCQUVELFcsMEJBQWM7QUFDVix5QkFBSyxTQUFMLEdBQWlCLENBQUMsS0FBSyxTQUF2QjtBQUNILGlCOzs4QkFFRCxNLG1CQUFPLEMsRUFBRyxNLEVBQVE7QUFDaEIsd0JBQUksVUFBVSxLQUFLLGNBQW5CO0FBQ0Esd0JBQUksUUFBUSxLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLE1BQXJCLENBQVo7QUFDQSx3QkFBSSxpQkFBaUIsU0FBakIsY0FBaUIsQ0FBQyxDQUFELEVBQU87QUFDeEIsNEJBQUcsRUFBRSxRQUFMLEVBQWU7QUFDWCxnQ0FBSSxNQUFNLFFBQVEsT0FBUixDQUFnQixDQUFoQixDQUFWO0FBQ0Esb0NBQVEsTUFBUixDQUFlLEdBQWYsRUFBb0IsQ0FBcEI7QUFDQSw4QkFBRSxRQUFGLEdBQWEsS0FBYjtBQUNILHlCQUpELE1BSU87QUFDSCw4QkFBRSxRQUFGLEdBQWEsSUFBYjtBQUNBLG9DQUFRLElBQVIsQ0FBYSxDQUFiO0FBQ0g7QUFDSixxQkFURDtBQVVBLHdCQUFHLEVBQUUsT0FBRixJQUFhLEVBQUUsT0FBbEIsRUFDSSxlQUFlLE1BQWYsRUFESixLQUVLLElBQUcsRUFBRSxRQUFMLEVBQWU7QUFDaEIsNEJBQUksU0FBUyxLQUFLLEdBQUwsQ0FBUyxLQUFULEVBQWdCLEtBQUssU0FBckIsQ0FBYjtBQUNBLDRCQUFJLFNBQVMsS0FBSyxHQUFMLENBQVMsS0FBVCxFQUFnQixLQUFLLFNBQXJCLElBQWtDLENBQS9DO0FBQ0EsNEJBQUksT0FBTyxLQUFLLE9BQUwsQ0FBYSxLQUFiLENBQW1CLE1BQW5CLEVBQTJCLE1BQTNCLENBQVg7QUFDQSwrQkFBTyxLQUFLLE1BQUwsQ0FBWTtBQUFBLG1DQUFLLEVBQUMsQ0FBQyxRQUFRLE9BQVIsQ0FBZ0IsQ0FBaEIsQ0FBUDtBQUFBLHlCQUFaLENBQVA7QUFDQSw2QkFBSyxPQUFMLENBQWEsYUFBSztBQUNkLDhCQUFFLFFBQUYsR0FBYSxJQUFiO0FBQ0Esb0NBQVEsSUFBUixDQUFhLENBQWI7QUFDSCx5QkFIRDtBQUlILHFCQVRJLE1BU0U7QUFDSCxnQ0FBUSxPQUFSLENBQWdCO0FBQUEsbUNBQUssRUFBRSxRQUFGLEdBQWEsS0FBbEI7QUFBQSx5QkFBaEI7QUFDQSxnQ0FBUSxNQUFSLEdBQWlCLENBQWpCO0FBQ0EsK0JBQU8sUUFBUCxHQUFrQixJQUFsQjtBQUNBLGdDQUFRLElBQVIsQ0FBYSxNQUFiO0FBQ0g7QUFDRCx5QkFBSyxTQUFMLEdBQWlCLEtBQUssT0FBTCxDQUFhLE9BQWIsQ0FBcUIsTUFBckIsQ0FBakI7QUFDRCxpQjs7OEJBWUMsZSw0QkFBZ0IsSSxFQUFNO0FBQ2xCLHdCQUFJLE9BQU8sZUFBWDtBQUNBLDJCQUFPLE9BQU8sQ0FBUCxHQUFXLENBQUMsSUFBWixHQUFtQixJQUExQjtBQUNBLHdCQUFHLFFBQVEsRUFBWCxFQUNJLFFBQVEscUJBQVIsQ0FESixLQUVLLElBQUcsT0FBTyxFQUFWLEVBQ0QsUUFBUSxtQkFBUixDQURDLEtBRUEsSUFBRyxPQUFPLEVBQVYsRUFDRCxRQUFRLHNCQUFSLENBREMsS0FFQSxJQUFHLE9BQU8sRUFBVixFQUNELFFBQVEseUJBQVIsQ0FEQyxLQUVBO0FBQ0QsZ0NBQVEsc0JBQVI7QUFDSDtBQUNELDJCQUFPLElBQVA7QUFDSCxpQjs7OEJBWUQsVSx5QkFBYTtBQUFBOztBQUNULHdCQUFJLFlBQVksU0FBWixTQUFZLElBQUs7QUFDakIsK0JBQU87QUFDSCxnQ0FBSSxFQUFFLEVBREg7QUFFSCxpQ0FBSyxFQUFFLEdBRko7QUFHSCxpQ0FBSyxFQUFFLEdBSEo7QUFJSCxpQ0FBSyxFQUFFLEdBSko7QUFLSCxpQ0FBSyxFQUFFLEdBTEo7QUFNSCxpQ0FBSyxFQUFFLEdBTko7QUFPSCxpQ0FBSyxFQUFFLEdBUEo7QUFRSCxrQ0FBTSxFQUFFLElBUkw7QUFTSCxxQ0FBUyxFQUFFLE9BVFI7QUFVSCxzQ0FBVSxFQUFFO0FBVlQseUJBQVA7QUFZSCxxQkFiRDtBQWNBLHdCQUFJLE9BQU87QUFDUCw4QkFBTSxLQUFLLElBREo7QUFFUCwrQkFBTyxLQUFLLEtBRkw7QUFHUCwrQkFBTyxLQUFLLEtBSEw7QUFJUCxtQ0FBVyxLQUFLLFNBSlQ7QUFLUCx5Q0FBaUIsS0FBSyxlQUxmO0FBTVAsd0NBQWdCLEtBQUssY0FBTCxDQUFvQixHQUFwQixDQUF3QjtBQUFBLG1DQUFLLE9BQUssT0FBTCxDQUFhLE9BQWIsQ0FBcUIsQ0FBckIsQ0FBTDtBQUFBLHlCQUF4QixDQU5UO0FBT1AsaUNBQVMsS0FBSyxPQVBQO0FBUVAsa0NBQVUsS0FBSyxRQVJSO0FBU1AsaUNBQVMsS0FBSyxPQUFMLENBQWEsR0FBYixDQUFpQixTQUFqQjtBQVRGLHFCQUFYO0FBV0Esd0JBQUksT0FBTyxLQUFLLFNBQUwsQ0FBZSxJQUFmLENBQVg7QUFDQSx3QkFBSSxjQUFjLDZCQUE2QixtQkFBbUIsSUFBbkIsQ0FBL0M7QUFDQSx3QkFBSSxPQUFPLElBQUksSUFBSixFQUFYO0FBQ0Esd0JBQUksV0FBVyxzQkFBc0IsS0FBSyxPQUFMLEVBQXRCLEdBQXVDLE9BQXREO0FBQ0Esd0JBQUksSUFBSSxTQUFTLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBUjtBQUNBLHNCQUFFLElBQUYsR0FBUyxVQUFVLFdBQW5CO0FBQ0Esc0JBQUUsUUFBRixHQUFhLFFBQWI7QUFDQSxzQkFBRSxLQUFGO0FBQ0gsaUI7OzhCQUVELFUseUJBQWE7QUFBQTs7QUFDVCx3QkFBSSxTQUFTLEVBQUUsT0FBRixDQUFiO0FBQ0Esd0JBQUksT0FBTyxPQUFPLENBQVAsRUFBVSxLQUFWLENBQWdCLENBQWhCLENBQVg7QUFDQSx3QkFBRyxDQUFDLElBQUosRUFBVTtBQUNOLDhCQUFNLGlCQUFOO0FBQ0E7QUFDSDtBQUNELHdCQUFJLFNBQVMsSUFBSSxVQUFKLEVBQWI7QUFDQSx3QkFBSSxhQUFhLFNBQWIsVUFBYSxPQUFRO0FBQ3JCLDRCQUFJLFVBQVUsRUFBZDtBQUNBLDZCQUFJLElBQUksSUFBSSxDQUFaLEVBQWUsSUFBSSxLQUFLLE1BQXhCLEVBQWdDLEdBQWhDLEVBQXFDO0FBQ2pDLGdDQUFJLElBQUksS0FBSyxDQUFMLENBQVI7QUFDQSxnQ0FBSSxZQUFZLElBQUksTUFBSixFQUFoQjtBQUNJLHNDQUFVLEVBQVYsR0FBZSxFQUFFLEVBQWpCO0FBQ0Esc0NBQVUsR0FBVixHQUFnQixFQUFFLEdBQWxCO0FBQ0Esc0NBQVUsR0FBVixHQUFnQixFQUFFLEdBQWxCO0FBQ0Esc0NBQVUsR0FBVixHQUFnQixFQUFFLEdBQWxCO0FBQ0Esc0NBQVUsR0FBVixHQUFnQixFQUFFLEdBQWxCO0FBQ0Esc0NBQVUsR0FBVixHQUFnQixFQUFFLEdBQWxCO0FBQ0Esc0NBQVUsR0FBVixHQUFnQixFQUFFLEdBQWxCO0FBQ0Esc0NBQVUsSUFBVixHQUFpQixFQUFFLElBQW5CLEVBQ0EsVUFBVSxPQUFWLEdBQW9CLEVBQUUsT0FEdEIsRUFFQSxVQUFVLFFBQVYsR0FBcUIsRUFBRSxRQUZ2QjtBQUdKLG9DQUFRLElBQVIsQ0FBYSxTQUFiO0FBQ0g7QUFDRCwrQkFBTyxPQUFQO0FBQ0gscUJBbEJEO0FBbUJBLDJCQUFPLE1BQVAsR0FBZ0IsVUFBQyxDQUFELEVBQU87QUFDbkIsNEJBQUksTUFBTSxLQUFLLEtBQUwsQ0FBVyxFQUFFLE1BQUYsQ0FBUyxNQUFwQixDQUFWO0FBQ0EsK0JBQUssSUFBTCxHQUFZLElBQUksSUFBaEI7QUFDQSwrQkFBSyxLQUFMLEdBQWEsSUFBSSxLQUFqQjtBQUNBLCtCQUFLLEtBQUwsR0FBYSxJQUFJLEtBQWpCO0FBQ0EsK0JBQUssT0FBTCxHQUFlLEVBQWY7QUFDQSwrQkFBSyxTQUFMLEdBQWlCLElBQUksU0FBckI7QUFDQSwrQkFBSyxPQUFMLEdBQWUsV0FBVyxJQUFJLE9BQWYsQ0FBZjtBQUNBLCtCQUFLLGVBQUwsR0FBdUIsSUFBSSxlQUEzQjtBQUNBLCtCQUFLLGNBQUwsR0FBc0IsSUFBSSxjQUFKLENBQW1CLEdBQW5CLENBQXVCO0FBQUEsbUNBQUssT0FBSyxPQUFMLENBQWEsQ0FBYixDQUFMO0FBQUEseUJBQXZCLENBQXRCO0FBQ0EsK0JBQUssT0FBTCxHQUFlLElBQUksT0FBbkI7QUFDQSwrQkFBSyxRQUFMLEdBQWdCLElBQUksUUFBcEI7QUFDSCxxQkFaRDtBQWFBLDJCQUFPLFVBQVAsQ0FBa0IsSUFBbEIsRUFBd0IsT0FBeEI7QUFDSCxpQjs7OEJBRUQsVyx3QkFBWSxDLEVBQUc7QUFDWCx5QkFBSyxRQUFMLEdBQWdCLEVBQUUsTUFBRixDQUFTLEtBQVQsQ0FBZSxDQUFmLEVBQWtCLElBQWxDO0FBQ0EseUJBQUssVUFBTDtBQUNILGlCOzs4QkFFRCxxQixrQ0FBc0IsUSxFQUFVLFEsRUFBVTtBQUN0Qyx5QkFBSyxXQUFMO0FBQ0gsaUI7OzhCQUVELFkseUJBQWEsUSxFQUFVLFEsRUFBVTtBQUM3Qix5QkFBSyxXQUFMO0FBQ0gsaUI7OzhCQUVELFkseUJBQWEsUSxFQUFVLFEsRUFBVTtBQUM3Qix5QkFBSyxXQUFMO0FBQ0gsaUI7OzhCQUVELFcsMEJBQWM7QUFDViw0QkFBTyxLQUFLLGVBQVo7QUFDSSw2QkFBSyxDQUFMO0FBQVEsbUNBQU8sS0FBSyxRQUFMLEVBQVA7QUFDUiw2QkFBSyxDQUFMO0FBQVEsbUNBQU8sS0FBSyxRQUFMLEVBQVA7QUFDUiw2QkFBSyxDQUFMO0FBQVEsbUNBQU8sS0FBSyxTQUFMLEVBQVA7QUFIWjtBQUtILGlCOzs4QkFFRCxNLHFCQUFTO0FBQ0wsd0JBQUksV0FBVyxFQUFmO0FBQ0EsK0JBQVcsS0FBSyxjQUFMLENBQW9CLEdBQXBCLENBQXdCLGtCQUFVO0FBQ3pDLCtCQUFPO0FBQ0gsK0JBQUcsT0FBTyxHQURQO0FBRUgsK0JBQUcsT0FBTyxHQUZQO0FBR0gsb0NBQVEsT0FBTyxFQUhaO0FBSUgsbUNBQU87QUFKSix5QkFBUDtBQU1ILHFCQVBVLENBQVg7QUFRQSwyQkFBTyxRQUFQO0FBQ0gsaUI7OzhCQUVELE0scUJBQVM7QUFDTCx3QkFBSSxXQUFXLEVBQWY7QUFDQSwrQkFBVyxLQUFLLGNBQUwsQ0FBb0IsR0FBcEIsQ0FBd0Isa0JBQVU7QUFDekMsK0JBQU87QUFDSCwrQkFBRyxPQUFPLEdBRFA7QUFFSCwrQkFBRyxPQUFPLEdBRlA7QUFHSCxvQ0FBUSxPQUFPLEVBSFo7QUFJSCxtQ0FBTztBQUpKLHlCQUFQO0FBTUgscUJBUFUsQ0FBWDtBQVFBLDJCQUFPLFFBQVA7QUFDSCxpQjs7OEJBRUQsUSx1QkFBVztBQUNQLHlCQUFLLGVBQUwsR0FBdUIsQ0FBdkI7QUFDQSx5QkFBSyxRQUFMLENBQWMsS0FBSyxNQUFMLEVBQWQ7QUFDSCxpQjs7OEJBRUQsUSx1QkFBVztBQUNQLHlCQUFLLGVBQUwsR0FBdUIsQ0FBdkI7QUFDQSx5QkFBSyxRQUFMLENBQWMsS0FBSyxNQUFMLEVBQWQ7QUFDSCxpQjs7OEJBRUQsUyx3QkFBWTtBQUNSLHdCQUFJLFdBQVcsRUFBZjtBQUNBLCtCQUFXLEtBQUssTUFBTCxHQUFjLE1BQWQsQ0FBcUIsS0FBSyxNQUFMLEVBQXJCLENBQVg7QUFDQSx5QkFBSyxlQUFMLEdBQXVCLENBQXZCO0FBQ0EseUJBQUssUUFBTCxDQUFjLFFBQWQ7QUFDSCxpQjs7OEJBRUQsSyxvQkFBUTtBQUNKLHlCQUFLLFNBQUwsR0FBaUIsQ0FBQyxLQUFLLFNBQXZCO0FBQ0EseUJBQUssUUFBTDtBQUNILGlCOzs4QkFFRCxLLG9CQUFRO0FBQ0oseUJBQUssU0FBTCxHQUFpQixDQUFDLEtBQUssU0FBdkI7QUFDQSx5QkFBSyxRQUFMO0FBQ0gsaUI7OzhCQUVELE0scUJBQVM7QUFDTCx3QkFBRyxLQUFLLFNBQUwsSUFBa0IsS0FBSyxTQUExQixFQUFxQztBQUNqQyw2QkFBSyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsNkJBQUssU0FBTCxHQUFpQixLQUFqQjtBQUNILHFCQUhELE1BR087QUFDSCw2QkFBSyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsNkJBQUssU0FBTCxHQUFpQixJQUFqQjtBQUNIO0FBQ0QseUJBQUssUUFBTDtBQUNILGlCOzs4QkFFRCxRLHFCQUFTLFEsRUFBVTtBQUFBOztBQUVmLHdCQUFHLFFBQUgsRUFBYSxLQUFLLFdBQUwsR0FBbUIsUUFBbkI7QUFDYiwrQkFBVyxLQUFLLFdBQWhCO0FBQ0Esd0JBQUcsQ0FBQyxRQUFELElBQWEsQ0FBQyxTQUFTLE1BQTFCLEVBQWtDOztBQUVsQyx1QkFBRyxNQUFILENBQVUsUUFBVixFQUFvQixTQUFwQixDQUE4QixHQUE5QixFQUFtQyxNQUFuQzs7QUFFQSx3QkFBSSxNQUFNLEdBQUcsTUFBSCxDQUFVLFFBQVYsQ0FBVjt3QkFDSSxRQUFRLEdBRFo7d0JBRUksU0FBUyxHQUZiO3dCQUdJLFVBQVU7QUFDTiw2QkFBSyxFQURDO0FBRU4sK0JBQU8sRUFGRDtBQUdOLGdDQUFRLEVBSEY7QUFJTiw4QkFBTTtBQUpBLHFCQUhkO3dCQVNJLFNBQVMsR0FBRyxLQUFILENBQVMsTUFBVCxHQUFrQixLQUFsQixDQUF3QixDQUFDLFFBQVEsSUFBVCxFQUFlLFFBQVEsUUFBUSxLQUEvQixDQUF4QixFQUNKLE1BREksQ0FDRyxDQUFDLElBQUQsRUFBTyxDQUFQLENBREgsQ0FUYjt3QkFXSSxTQUFTLEdBQUcsS0FBSCxDQUFTLE1BQVQsR0FBa0IsS0FBbEIsQ0FBd0IsQ0FBQyxTQUFTLFFBQVEsR0FBbEIsRUFBdUIsUUFBUSxNQUEvQixDQUF4QixFQUNKLE1BREksQ0FDRyxDQUFDLElBQUQsRUFBTyxDQUFQLENBREgsQ0FYYjt3QkFjSSxRQUFRLEdBQUcsR0FBSCxDQUFPLElBQVAsR0FDSCxLQURHLENBQ0csTUFESCxFQUVILE1BRkcsQ0FFSSxRQUZKLEVBR0gsUUFIRyxDQUdNLENBSE4sRUFJSCxLQUpHLENBSUcsQ0FKSCxFQUtILGFBTEcsQ0FLVyxDQUFDLE1BQUQsR0FBVSxRQUFRLE1BQWxCLEdBQTJCLFFBQVEsR0FMOUMsRUFNSCxhQU5HLENBTVcsQ0FOWCxFQU9ILFdBUEcsQ0FPUyxFQVBULEVBUUgsYUFSRyxDQVFXLElBUlgsQ0FkWjt3QkF3QkksUUFBUSxHQUFHLEdBQUgsQ0FBTyxJQUFQLEdBQ0gsS0FERyxDQUNHLE1BREgsRUFFSCxNQUZHLENBRUksT0FGSixFQUdILFFBSEcsQ0FHTSxDQUhOLEVBSUgsS0FKRyxDQUlHLENBSkgsRUFLSCxhQUxHLENBS1csQ0FBQyxLQUFELEdBQVMsUUFBUSxJQUFqQixHQUF3QixRQUFRLEtBTDNDLEVBTUgsYUFORyxDQU1XLENBTlgsRUFPSCxXQVBHLENBT1MsRUFQVCxFQVFILGFBUkcsQ0FRVyxJQVJYLENBeEJaOztBQWtDQSx3QkFBSSxZQUFZLFNBQVMsTUFBVCxDQUFnQjtBQUFBLCtCQUN4QixPQUFLLFNBQUwsSUFBa0IsRUFBRSxLQUFGLElBQVcsQ0FBN0IsSUFDQSxPQUFLLFNBQUwsSUFBa0IsRUFBRSxLQUFGLElBQVcsQ0FGTDtBQUFBLHFCQUFoQixDQUFoQjtBQUdBLHdCQUFJLGFBQWEsR0FBRyxJQUFILEdBQVUsR0FBVixDQUFjO0FBQUEsK0JBQUssRUFBRSxLQUFQO0FBQUEscUJBQWQsRUFBNEIsT0FBNUIsQ0FBb0MsU0FBcEMsQ0FBakI7QUFDQSx3QkFBSSxVQUFVLFNBQVYsT0FBVTtBQUFBLCtCQUFLLE1BQU0sR0FBRyxJQUFILENBQVEsSUFBUixDQUFhLEVBQUUsTUFBRixDQUFTLEdBQVQsQ0FBYTtBQUFBLG1DQUFLLENBQUMsT0FBTyxFQUFFLENBQVQsQ0FBRCxFQUFjLE9BQU8sRUFBRSxDQUFULENBQWQsQ0FBTDtBQUFBLHlCQUFiLENBQWIsRUFBNEQsSUFBNUQsQ0FBaUUsR0FBakUsQ0FBTixHQUE4RSxHQUFuRjtBQUFBLHFCQUFkOztBQUVBLHdCQUFJLE9BQU8sSUFBSSxTQUFKLENBQWMsTUFBZCxFQUNOLElBRE0sQ0FDRCxVQURDLEVBRU4sSUFGTSxDQUVELEdBRkMsRUFFSSxPQUZKLEVBR04sS0FITSxHQUdFLE1BSEYsQ0FHUyxNQUhULEVBSUYsS0FKRSxDQUlJLE1BSkosRUFJWTtBQUFBLCtCQUFLLEVBQUUsR0FBRixJQUFTLENBQVQsR0FBYSxTQUFiLEdBQXlCLFNBQTlCO0FBQUEscUJBSlosRUFLRixLQUxFLENBS0ksY0FMSixFQUtvQixLQUxwQixFQU1GLEtBTkUsQ0FNSSxRQU5KLEVBTWM7QUFBQSwrQkFBSyxFQUFFLEdBQUYsSUFBUyxDQUFULEdBQWEsU0FBYixHQUF5QixTQUE5QjtBQUFBLHFCQU5kLEVBT0YsS0FQRSxDQU9JLGNBUEosRUFPb0IsQ0FQcEIsRUFRRixJQVJFLENBUUcsR0FSSCxFQVFRLE9BUlIsQ0FBWDs7QUFVQSx3QkFBSSxTQUFTLElBQUksTUFBSixDQUFXLE9BQVgsRUFDUixJQURRLENBQ0gsT0FERyxFQUNNLFFBRE4sRUFFUixJQUZRLENBRUgsV0FGRyxFQUVVLGtCQUFrQixTQUFTLFFBQVEsTUFBbkMsSUFBNkMsR0FGdkQsQ0FBYjs7QUFJQSwyQkFBTyxJQUFQLENBQVksS0FBWjs7QUFFQSwyQkFDSyxNQURMLENBQ1ksTUFEWixFQUVLLElBRkwsQ0FFVSxHQUZWLEVBRWUsRUFGZixFQUdLLElBSEwsQ0FHVSxHQUhWLEVBR2UsUUFBUSxDQUh2QixFQUlLLElBSkwsQ0FJVSxPQUpWLEVBSW1CLG9DQUpuQixFQUtLLElBTEwsQ0FLVSxJQUxWOztBQU9BLHdCQUFJLFNBQVMsSUFBSSxNQUFKLENBQVcsT0FBWCxFQUNSLElBRFEsQ0FDSCxPQURHLEVBQ00sUUFETixFQUVSLElBRlEsQ0FFSCxXQUZHLEVBRVUsZ0JBQWdCLFFBQVEsUUFBUSxLQUFoQyxJQUF5QyxLQUZuRCxDQUFiOztBQUlBLDJCQUFPLElBQVAsQ0FBWSxLQUFaOztBQUVBLDJCQUNLLE1BREwsQ0FDWSxNQURaLEVBRUssSUFGTCxDQUVVLEdBRlYsRUFFZSxTQUFTLENBRnhCLEVBR0ssSUFITCxDQUdVLEdBSFYsRUFHZSxFQUhmLEVBSUssSUFKTCxDQUlVLE9BSlYsRUFJbUIsb0NBSm5CLEVBS0ssSUFMTCxDQUtVLElBTFY7O0FBT0Esd0JBQUksV0FBVyxHQUFHLEdBQUgsQ0FBTyxJQUFQLEdBQ1YsQ0FEVSxDQUNSO0FBQUEsK0JBQUssT0FBTyxFQUFFLENBQVQsQ0FBTDtBQUFBLHFCQURRLEVBRVYsQ0FGVSxDQUVSO0FBQUEsK0JBQUssT0FBTyxFQUFFLENBQVQsQ0FBTDtBQUFBLHFCQUZRLEVBR1YsV0FIVSxDQUdFLFFBSEYsQ0FBZjs7QUFLQSx3QkFBSSxjQUFjLElBQUksU0FBSixDQUFjLFFBQWQsRUFDYixJQURhLENBQ1IsUUFEUSxFQUViLEtBRmEsRUFBbEI7O0FBSUEsd0JBQUksY0FBYyxZQUNiLE1BRGEsQ0FDTixHQURNLENBQWxCOztBQUdBLGdDQUNTLE1BRFQsQ0FDZ0IsUUFEaEIsRUFFYSxJQUZiLENBRWtCLElBRmxCLEVBRXdCO0FBQUEsK0JBQUssT0FBTyxFQUFFLENBQVQsQ0FBTDtBQUFBLHFCQUZ4QixFQUdhLElBSGIsQ0FHa0IsSUFIbEIsRUFHd0I7QUFBQSwrQkFBSyxPQUFPLEVBQUUsQ0FBVCxDQUFMO0FBQUEscUJBSHhCLEVBSWEsS0FKYixDQUltQixNQUpuQixFQUkyQjtBQUFBLCtCQUFLLEVBQUUsS0FBRixLQUFZLENBQVosR0FBZ0IsU0FBaEIsR0FBNEIsU0FBakM7QUFBQSxxQkFKM0IsRUFLYSxJQUxiLENBS2tCLEdBTGxCLEVBS3VCLENBTHZCOztBQU9BLGdDQUNLLE1BREwsQ0FDWSxNQURaLEVBRUssSUFGTCxDQUVVLEdBRlYsRUFFZTtBQUFBLCtCQUFLLE9BQU8sRUFBRSxDQUFULElBQWMsRUFBbkI7QUFBQSxxQkFGZixFQUdLLElBSEwsQ0FHVSxHQUhWLEVBR2U7QUFBQSwrQkFBSyxPQUFPLEVBQUUsQ0FBVCxJQUFjLENBQW5CO0FBQUEscUJBSGYsRUFJSyxJQUpMLENBSVU7QUFBQSwrQkFBSyxFQUFFLE1BQVA7QUFBQSxxQkFKVjs7QUFNQSx3QkFBSSxhQUFhLENBQ2IsRUFBRSxPQUFPLFNBQVQsRUFBb0IsT0FBTyxLQUFLLEtBQWhDLEVBQXVDLFFBQVEsQ0FBL0MsRUFEYSxFQUViLEVBQUUsT0FBTyxTQUFULEVBQW9CLE9BQU8sS0FBSyxLQUFoQyxFQUF1QyxRQUFRLENBQS9DLEVBRmEsQ0FBakI7O0FBS0Esd0JBQUksU0FBUyxJQUNWLE1BRFUsQ0FDSCxHQURHLEVBRVYsSUFGVSxDQUVMLE9BRkssRUFFSSxRQUZKLEVBR1YsSUFIVSxDQUdMLFdBSEssRUFHUSxlQUFlLEtBQWYsR0FBdUIsR0FBdkIsR0FBNkIsUUFBUSxHQUFyQyxHQUEyQyxHQUhuRCxDQUFiOztBQVlBLHdCQUFJLGNBQWMsT0FDYixTQURhLENBQ0gsUUFERyxFQUViLElBRmEsQ0FFUixVQUZRLEVBR2IsS0FIYSxFQUFsQjs7QUFLQSx3QkFBSSxRQUFRLFlBQ1AsTUFETyxDQUNBLEdBREEsRUFFUCxJQUZPLENBRUYsT0FGRSxFQUVPLE9BRlAsRUFHUCxJQUhPLENBR0YsV0FIRSxFQUdXO0FBQUEsK0JBQUssbUJBQW1CLEVBQUUsTUFBRixHQUFXLEVBQVgsR0FBZ0IsRUFBbkMsSUFBeUMsR0FBOUM7QUFBQSxxQkFIWCxFQUlQLElBSk8sQ0FJRixHQUpFLEVBSUc7QUFBQSwrQkFBSyxFQUFFLE1BQUYsR0FBVyxFQUFoQjtBQUFBLHFCQUpILENBQVo7O0FBTUEsMEJBQ0csTUFESCxDQUNVLFFBRFYsRUFFRyxJQUZILENBRVEsR0FGUixFQUVhLENBRmIsRUFHRyxLQUhILENBR1MsTUFIVCxFQUdpQjtBQUFBLCtCQUFLLEVBQUUsS0FBUDtBQUFBLHFCQUhqQjs7QUFLQSwwQkFBTSxNQUFOLENBQWEsTUFBYixFQUNHLElBREgsQ0FDUSxHQURSLEVBQ2EsRUFEYixFQUVHLElBRkgsQ0FFUSxHQUZSLEVBRWEsQ0FGYixFQUdHLElBSEgsQ0FHUTtBQUFBLCtCQUFLLEVBQUUsS0FBUDtBQUFBLHFCQUhSOztBQUtBLHdCQUFJLFNBQUosQ0FBYyxRQUFkLEVBQ0ssSUFETCxDQUNVO0FBQUEsK0JBQUssRUFBRSxNQUFQO0FBQUEscUJBRFY7QUFFSCxpQjs7OEJBRUQsWSwyQkFBZTtBQUNYLHdCQUFJLFVBQVUsbUJBQW1CLEtBQW5CLENBQXlCLEVBQXpCLENBQWQ7QUFDQSx3QkFBSSxRQUFRLEdBQVo7QUFDQSx5QkFBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLENBQXBCLEVBQXVCLEdBQXZCLEVBQTZCO0FBQ3pCLGlDQUFTLFFBQVEsS0FBSyxLQUFMLENBQVcsS0FBSyxNQUFMLEtBQWdCLEVBQTNCLENBQVIsQ0FBVDtBQUNIO0FBQ0QsMkJBQU8sS0FBUDtBQUNILGlCOzs7O3dDQTFXZ0I7QUFDYiwrQkFBTyxLQUFLLGNBQUwsQ0FBb0IsUUFBcEIsQ0FBNkIsc0JBQTdCLENBQVA7QUFDSDs7O3dDQUdvQjtBQUNqQiwrQkFBTyxLQUFLLGVBQUwsQ0FBcUIsS0FBSyxPQUExQixDQUFQO0FBQ0g7Ozt3Q0FvQmtCO0FBQ2YsZ0NBQU8sS0FBSyxlQUFaO0FBQ0ksaUNBQUssQ0FBTDtBQUFRLHVDQUFPLEtBQUssS0FBWjtBQUNSLGlDQUFLLENBQUw7QUFBUSx1Q0FBTyxLQUFLLEtBQVo7QUFDUixpQ0FBSyxDQUFMO0FBQVEsdUNBQU8sS0FBSyxLQUFMLEdBQWEsR0FBYixHQUFtQixLQUFLLEtBQS9CO0FBQ1I7QUFBUyx1Q0FBTyxFQUFQO0FBSmI7QUFNSDs7OzsrRkE1TEYsVTs7OzJCQUE0QixFOztxRkFPNUIsVTs7OzJCQUFtQixHOztxRkFDbkIsVTs7OzJCQUFtQixHIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZVJvb3QiOiIvc3JjIn0=
