import {computedFrom, inject} from 'aurelia-framework';
import {observable} from 'aurelia-framework';
import {Letter} from 'letter';
import * as d3 from 'd3';
import $ from 'jquery';
import 'jquery-ui';
import 'bootstrap';

export class App {
  showMouth = false;
  showAreaO = false;
  showAreaL = false;
  showCompArea = false;
  currentData = [];
  title = 'QualiPhon';
  @observable currentLetters = [];
  currentAnalysis = 0;
  compArea = 0;
  compAreaSide = 0;
  compAll = 0;
  compDesc = "";
  lang = "";
  @observable nameO = "O";
  @observable nameL = "L";
  letters = [ 
      new Letter("a", true), 
      new Letter("b", true), 
      new Letter("c", true), 
      new Letter("d", true), 
      new Letter("e", true), 
      new Letter("f", true), 
      new Letter("g", true), 
      new Letter("h", true), 
      new Letter("i", true), 
      new Letter("j", true), 
      new Letter("k", true), 
      new Letter("l", true), 
      new Letter("m", true), 
      new Letter("n", true), 
      new Letter("o", true), 
      new Letter("p", true), 
      new Letter("q", true), 
      new Letter("r", true), 
      new Letter("s", true), 
      new Letter("t", true), 
      new Letter("u", true), 
      new Letter("v", true), 
      new Letter("w", true), 
      new Letter("x", true), 
      new Letter("y", true), 
      new Letter("z", true) ];

  attached() {
      $('.draggable').draggable();
      $('.draggable').resizable();
  }

  compare(comp) {
      this.currentLetters.forEach(currentLetter => {
          if(!currentLetter) return;
          currentLetter.compare(comp, true);
      });
  }

  compareAll(comp) {
      var res = 0;
      var all = (func) => {
          return this.letters.reduce((b, c) => {
              return b + func(c);
          }, 0) / this.letters.length;
      }
      switch(comp) {
          case "f1f2":
                res = all(l => l.compare("f1f2"));
                this.compDesc = "All F1,F2";
                break;
          case "f1f3":
                res = all(l => {
                        var c1 = l.compare("f1");
                        var c2 = l.compare("f2");
                        var c3 = l.compare("f3");
                        return (c1 + c2 + c3) / 3; });
                this.compDesc = "All F1-F3";
                break;
      }
      this.compAll = Math.abs(Math.round(res));
  }
  
  toggleArea() {
      this.showCompArea = !this.showCompArea;
  }
  
  compareArea() {
      var dataO = this._dataO();
      var dataL = this._dataL();
      var hullO = d3.geom.hull(dataO.map(e => [e.x, e.y]));
      var hullL = d3.geom.hull(dataL.map(e => [e.x, e.y]));
      var areaO = d3.geom.polygon(hullO).area();
      var areaL = d3.geom.polygon(hullL).area();
      
      var max = d3.max([areaO, areaL]);
      var min = d3.min([areaO, areaL]);
      
      if(max === areaO) this.compAreaSide = 1;
      else this.compAreaSide = 2;
      
      this.compArea = Math.abs(Math.round(min / max * 100));
  }

  removeLetter(index, conf = true) {
    if(!~index) return;
    var letter = this.letters[index];
    if(conf) {
        var confirmation = confirm("Are you sure that you want to remove this letter?");
        if(confirmation)
            this.letters.splice(index, 1);
    } else
        this.letters.splice(index, 1);
  }

  removeSelected() {
    var confirmation = confirm("Are you sure you want to remove all selected letters?");
    if(confirmation)
        this.currentLetters.forEach(l => this.removeLetter(this.letters.indexOf(l), false));
    this.currentLetters = [];
  }

  addLetter() {
      this.letters.push(new Letter());
  }

  toggleMouth() {
      this.showMouth = !this.showMouth
  }

  choose(e, letter) {
    var letters = this.currentLetters;
    var index = this.letters.indexOf(letter);
    var toggleSelected = (l) => {
        if(l.selected) {
            var idx = letters.indexOf(l);
            letters.splice(idx, 1);
            l.selected = false;
        } else {
            l.selected = true;
            letters.push(l);
        }
    }
    if(e.ctrlKey || e.metaKey)
        toggleSelected(letter);
    else if(e.shiftKey) {
        var minIdx = Math.min(index, this.lastIndex);
        var maxIdx = Math.max(index, this.lastIndex) + 1;
        var part = this.letters.slice(minIdx, maxIdx);
        part = part.filter(l => !~letters.indexOf(l));
        part.forEach(l => {
            l.selected = true
            letters.push(l);
        });
    } else {
        letters.forEach(l => l.selected = false);
        letters.length = 0;    
        letter.selected = true;
        letters.push(letter);
    }
    this.lastIndex = this.letters.indexOf(letter);
  }

    @computedFrom('compAll')
    get is_success() {
        return this.progress_class.includes("progress-bar-success");
    }

    @computedFrom('compAll')
    get progress_class() {
        return this._progress_class(this.compAll);
    }
    
    _progress_class(vari) {
        var prog = "progress-bar ";
        vari = vari < 0 ? -vari : vari;
        if(vari <= 50)
            prog += "progress-bar-danger";
        else if(vari < 65)
            prog += "progress-bar-rose";
        else if(vari < 80)
            prog += "progress-bar-warning";
        else if(vari < 90)
            prog += "progress-bar-lightgreen";
        else {
            prog += "progress-bar-success";
        }
        return prog;
    }

    @computedFrom('currentAnalysis', 'nameO', 'nameL')
    get analysisName() {
        switch(this.currentAnalysis) {
            case 1: return this.nameO;
            case 2: return this.nameL;
            case 3: return this.nameO + "/" + this.nameL;
            default: return "";
        }
    }

    exportDump() {
        var mapLetter = l => {
            return {
                ch: l.ch,
                f1o: l.f1o,
                f2o: l.f2o,
                f3o: l.f3o,
                f1l: l.f1l,
                f2l: l.f2l,
                f3l: l.f3l,
                comp: l.comp,
                compRes: l.compRes,
                selected: l.selected
            }
        }
        var data = {
            lang: this.lang,
            nameO: this.nameO,
            nameL: this.nameL,
            showMouth: this.showMouth,
            currentAnalysis: this.currentAnalysis,
            currentLetters: this.currentLetters.map(l => this.letters.indexOf(l)),
            compAll: this.compAll,
            compDesc: this.compDesc,
            letters: this.letters.map(mapLetter)
        }
        var json = JSON.stringify(data);
        var fileContent = "text/json;charset=UTF-8," + encodeURIComponent(json);
        var date = new Date();
        var fileName = "qualiphon_export_" + date.getTime() + ".json";
        var a = document.createElement('a');
        a.href = 'data:' + fileContent;
        a.download = fileName;
        a.click();
    }

    importDump() {
        var $files = $('#file')
        var file = $files[0].files[0];
        if(!file) {
            alert("No file chosen.");
            return;
        }
        var reader = new FileReader();
        var mapLetters = lets => {
            var letters = [];
            for(let i = 0; i < lets.length; i++) {
                var l = lets[i];
                var newLetter = new Letter();
                    newLetter.ch = l.ch;
                    newLetter.f1o = l.f1o;
                    newLetter.f2o = l.f2o;
                    newLetter.f3o = l.f3o;
                    newLetter.f1l = l.f1l;
                    newLetter.f2l = l.f2l;
                    newLetter.f3l = l.f3l;
                    newLetter.comp = l.comp,
                    newLetter.compRes = l.compRes,
                    newLetter.selected = l.selected
                letters.push(newLetter);
            }
            return letters;
        }
        reader.onload = (e) => {
            var res = JSON.parse(e.target.result);
            this.lang = res.lang;
            this.nameO = res.nameO;
            this.nameL = res.nameL;
            this.letters = [];
            this.showMouth = res.showMouth;
            this.letters = mapLetters(res.letters);
            this.currentAnalysis = res.currentAnalysis;
            this.currentLetters = res.currentLetters.map(l => this.letters[l]);
            this.compAll = res.compAll;
            this.compDesc = res.compDesc;
        }
        reader.readAsText(file, "UTF-8");
    }

    fileChanged(e) {
        this.fileName = e.target.files[0].name; 
        this.importDump();
    }

    currentLettersChanged(newValue, oldValue) {
        this.updateChart();
    }

    nameOChanged(newValue, oldValue) {
        this.updateChart();
    }

    nameLChanged(newValue, oldValue) {
        this.updateChart();
    }

    updateChart() {
        switch(this.currentAnalysis) {
            case 1: return this.analyseO();
            case 2: return this.analyseL();
            case 3: return this.analyseOL();
        }
    }
    
    _dataO() {
        var lineData = [];
        lineData = this.currentLetters.map(letter => {
            return { 
                x: letter.f2o,
                y: letter.f1o,
                letter: letter.ch,
                which: 1
            };
        });
        return lineData;
    }
    
    _dataL() {
        var lineData = [];
        lineData = this.currentLetters.map(letter => {
            return { 
                x: letter.f2l,
                y: letter.f1l,
                letter: letter.ch,
                which: 2
            };
        });
        return lineData;
    }

    analyseO() {
        this.currentAnalysis = 1;
        this._analyse(this._dataO());
    }
    
    analyseL() {
        this.currentAnalysis = 2;
        this._analyse(this._dataL());
    }

    analyseOL() {
        var lineData = [];
        lineData = this._dataO().concat(this._dataL());
        this.currentAnalysis = 3;
        this._analyse(lineData);
    }
    
    areaO() {
        this.showAreaO = !this.showAreaO;
        this._analyse();
    }
    
    areaL() {
        this.showAreaL = !this.showAreaL;
        this._analyse();
    }
    
    areaOL() {
        if(this.showAreaL || this.showAreaO) {
            this.showAreaO = false;
            this.showAreaL = false;
        } else {
            this.showAreaO = true;
            this.showAreaL = true;
        }
        this._analyse();
    }

    _analyse(lineData) {
        
        if(lineData) this.currentData = lineData;
        lineData = this.currentData;
        if(!lineData || !lineData.length) return;

        d3.select("#chart").selectAll("*").remove();

        var vis = d3.select("#chart"),
            WIDTH = 600,
            HEIGHT = 300,
            MARGINS = {
                top: 20,
                right: 50,
                bottom: 20,
                left: 20
            },
            xRange = d3.scale.linear().range([MARGINS.left, WIDTH - MARGINS.right])
                .domain([3500, 0]),
            yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom])
                .domain([1200, 0]),

            xAxis = d3.svg.axis()
                .scale(xRange)
                .orient("bottom")
                .tickSize(1)
                .ticks(7)
                .innerTickSize(-HEIGHT + MARGINS.bottom + MARGINS.top)
                .outerTickSize(0)
                .tickPadding(10)
                .tickSubdivide(true),

            yAxis = d3.svg.axis()
                .scale(yRange)
                .orient("right")
                .tickSize(1)
                .ticks(7)
                .innerTickSize(-WIDTH + MARGINS.left + MARGINS.right)
                .outerTickSize(0)
                .tickPadding(10)
                .tickSubdivide(true);
                
        var groupData = lineData.filter(d => 
                this.showAreaO && d.which == 1 ||
                this.showAreaL && d.which == 2);
        var hullGroups = d3.nest().key(d => d.which).entries(groupData);
        var dGroups = d => "M" + d3.geom.hull(d.values.map(e => [xRange(e.x), yRange(e.y)])).join("L") + "Z";

        var hull = vis.selectAll("path")
            .data(hullGroups)
            .attr("d", dGroups)
            .enter().insert("path")
                .style("fill", d => d.key == 2 ? "#8bc34a" : "#03a9f4")
                .style("fill-opacity", "0.4")
                .style("stroke", d => d.key == 2 ? "#8bc34a" : "#03a9f4")
                .style("stroke-width", 2)
                .attr("d", dGroups);

        var xGroup = vis.append('svg:g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')

        xGroup.call(xAxis);

        xGroup
            .append('text')
            .attr("y", 50)
            .attr("x", WIDTH / 2)
            .attr("style", "font-weight: bold; font-size: 14pt")
            .text("F2")

        var yGroup = vis.append('svg:g')
            .attr('class', 'y axis')
            .attr('transform', 'translate(' + (WIDTH - MARGINS.right) + ',0)')

        yGroup.call(yAxis);

        yGroup
            .append('text')
            .attr("y", HEIGHT / 2)
            .attr("x", 50)
            .attr("style", "font-weight: bold; font-size: 14pt")
            .text("F1")

        var lineFunc = d3.svg.line()
            .x(d => xRange(d.x))
            .y(d => yRange(d.y))
            .interpolate('linear');

        var enterCircle = vis.selectAll("circle")
            .data(lineData)
            .enter();

        var circleGroup = enterCircle
            .append("g");

        circleGroup
                .append("circle")
                    .attr("cx", d => xRange(d.x))
                    .attr("cy", d => yRange(d.y))
                    .style("fill", d => d.which === 1 ? "#3f51b5" : "#4caf50")
                    .attr("r", 5);

        circleGroup
            .append("text")
            .attr("x", d => xRange(d.x) + 10)
            .attr("y", d => yRange(d.y) + 5)
            .text(d => d.letter)

        var legendData = [
            { color: "#3f51b5", title: this.nameO, offset: 0 },
            { color: "#4caf50", title: this.nameL, offset: 1 }
        ];

        var legend = vis
          .append("g")
          .attr("class", "legend")
          .attr("transform", "translate(" + WIDTH + "," + MARGINS.top + ")")

        //legend
        //    .append("rect")
        //    .attr("height", 60)
        //    .attr("width", 100)
        //    .attr("y", -9)
        //    .attr("x", -7)

        var enterLegend = legend
            .selectAll(".entry")
            .data(legendData)
            .enter();

        var entry = enterLegend
            .append("g")
            .attr("class", "entry")
            .attr("transform", d => "translate(10," + (d.offset * 20 + 10) + ")")
            .attr("y", d => d.offset * 20)

        entry
          .append("circle")
          .attr("r", 7)
          .style("fill", d => d.color);

        entry.append("text")
          .attr("x", 20)
          .attr("y", 7)
          .text(d => d.title);

        vis.selectAll("circle")
            .text(d => d.letter)
    }

    _randomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}
