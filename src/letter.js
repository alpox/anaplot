import {computedFrom} from 'aurelia-framework';

export class Letter {

    constructor(ch = "", rnd = false) {
        this.ch = ch;
        this.f1o = "";
        this.f2o = "";
        this.f3o = "";
        this.f1l = "";
        this.f2l = "";
        this.f3l = "";
        this.comp = "";
        this.compRes = 0;

        this.selected = false;

        if(rnd)
            this.set_random();
    }

    _compare(l, o) {
        var vari; 
        var f1l = parseInt(l);
        var f1o = parseInt(o);
        if(!f1l || !f1o) {
            return 0;
        }
        if(f1l < f1o)
            vari = f1l / f1o;
        else {
            vari = (f1l - f1o) / f1o;
            if(vari <= 1)
                vari = 1 - vari;
        }

        if(vari > 1) vari = 0;
        else if(vari < 0) vari = 0;
        
        return vari * 100;
    }

    _compareTwo(l1, l2, o1, o2) {
        var comp1 = this._compare(l1, o1);
        var comp2 = this._compare(l2, o2);
        return (comp1 + comp2) / 2;
    }

    compare(indication, set = false) {
        var res = 0;
        switch(indication) {
            case "f1":
                res = this._compare(this.f1l, this.f1o);
                if(set) this.comp = "F1";
                break;
            case "f2":
                res = this._compare(this.f2l, this.f2o);
                if(set) this.comp = "F2";
                break;
            case "f3":
                res = this._compare(this.f3l, this.f3o);
                if(set) this.comp = "F3";
                break;
            case "f1f2":
                res = this._compareTwo(this.f1l, this.f2l, this.f1o, this.f2o);
                if(set) this.comp = "F1,F2";
                break;
            case "f2f3":
                res = this._compareTwo(this.f2l, this.f3l, this.f2o, this.f3o);
                if(set) this.comp = "F2,F3";
                break;
            case "f1f3":
                res = this._compareTwo(this.f1l, this.f3l, this.f1o, this.f3o);
                if(set) this.comp = "F1,F3";
                break;
        }
        res = Math.abs(Math.round(res));
        if(set) this.compRes = res;
        return res;
    }

    set_random() {
        this.f1o = Math.round(Math.random()*1000);
        this.f2o = Math.round(Math.random()*3000);
        this.f3o = Math.round(Math.random()*3000);
        this.f1l = Math.round(Math.random()*this.f1o/3) + this.f1o;
        this.f2l = Math.round(Math.random()*this.f2o/3) + this.f2o;
        this.f3l = Math.round(Math.random()*this.f3o/3) + this.f3o;
    }

    @computedFrom('compRes')
    get is_success() {
        return this.progress_class.includes("progress-bar-success");
    }

    @computedFrom('compRes')
    get progress_class() {
        var vari = this.compRes;
        var prog = "progress-bar ";
        vari = vari < 0 ? -vari : vari;
        if(vari < 50)
            prog += "progress-bar-danger";
        else if(vari < 65)
            prog += "progress-bar-rose";
        else if(vari < 80)
            prog += "progress-bar-warning";
        else if(vari < 90)
            prog += "progress-bar-lightgreen";
        else {
            prog += "progress-bar-success";
        }
        return prog;
    }

    @computedFrom('selected')
    get selected_class() {
        return this.selected ? 'selected' : '';
    }
}
